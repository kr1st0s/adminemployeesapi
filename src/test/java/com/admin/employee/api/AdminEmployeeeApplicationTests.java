package com.admin.employee.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.domain.Person;
import com.admin.employee.api.domain.Position;
import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.entity.PersonEntity;
import com.admin.employee.api.entity.PositionEntity;
import com.admin.employee.api.mapper.impl.EmployeeToEmployeeEntityMapper;
import com.admin.employee.api.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@ContextConfiguration(classes = AdminEmployeeeApplication.class)
class AdminEmployeeeApplicationTests {

	@Autowired
	WebApplicationContext context;

	private MockMvc mockMvc;

	@Autowired
	EmployeeService employeeService;

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	void addEmployee() throws Exception {
		EmployeeEntity e = new EmployeeEntity();
		e.setSalary(1500000);
		e.setPerson(new PersonEntity("Oscar", "Salazar", "carrera # 17-20", "111111", "Mocoa"));
		e.getPerson().setId(4L);
		e.setPosition(new PositionEntity(1L, "DEV", null));

		String content = new ObjectMapper().writeValueAsString(e);

		mockMvc.perform(post("/api/admin/v1/employee/save").contentType(MediaType.APPLICATION_JSON).content(content))
				.andExpect(status().isOk());
	}

	@Test
	void updateEmployee() throws Exception {
		Employee e = employeeService.findEmployeeListByPositionOrPersonName(0l, null).get(0);
		String content = new ObjectMapper().writeValueAsString(new EmployeeToEmployeeEntityMapper().mapper(e));
		mockMvc.perform(put("/api/admin/v1/employee/update").contentType(MediaType.APPLICATION_JSON).content(content))
				.andExpect(status().isOk());
	}

	@Test
	void getEmployee() throws Exception {
		mockMvc.perform(get("/api/admin/v1/employee/find?chargeId&personName=").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	void deleteEmployee() throws Exception {
		Employee employee = new Employee();
		employee.setSalary(1800000);
		employee.setPerson(new Person());
		employee.getPerson().setId(6L);
		employee.setPosition(new Position());
		employee.getPosition().setId(1L);
		employee = employeeService.save(employee);

		mockMvc.perform(delete("/api/admin/v1/employee/delete").contentType(MediaType.APPLICATION_JSON)
				.queryParam("employeeId", employee.getId().toString())).andExpect(status().isOk());
	}

}
