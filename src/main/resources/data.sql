INSERT INTO TBL_POSITION (position_name) VALUES 
('DEV'),
('QA'),
('SCRUM');
  
INSERT INTO TBL_PERSON (first_name,last_name,address,cell_phone,city_name) VALUES
  ('Lenny','Escobar','Carrera 14 #18-20','111111', 'Popayan'),
  ('Pedro','Perez','Calle  23-35','222222', 'Medellin'),
  ('Juan','Terreno','Calle  11-16','99999', 'Cali'),
  ('Carlos','Montolla','Calle  17-35','444444', 'Bogota'),
  ('Carlos','Lopez','Calle  76-35','55555', 'Medellin'),
  ('Diego','Sanchez','Carrera  76-35','55555', 'Medellin'),
  ('Carlos','Lopez','Calle  76-35','55555', 'Medellin'),
  ('Lenny','Escobar','Carrera 14# 88-20','333333', 'Bogota');