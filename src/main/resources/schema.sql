drop table if exists TBL_EMPLOYEE;
drop table if exists TBL_PERSON;
drop table if exists TBL_POSITION;

CREATE TABLE TBL_PERSON (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  address VARCHAR(250) NOT NULL,
  cell_phone VARCHAR(250) DEFAULT NULL,
  city_name VARCHAR(250) DEFAULT NULL
); 

CREATE TABLE TBL_POSITION (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  position_name VARCHAR(250) NOT NULL
);
  
CREATE TABLE TBL_EMPLOYEE (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  salary INTEGER NOT NULL,
  person_id INT NOT NULL,
  position_id INT NOT NULL,
  foreign key (person_id) references TBL_PERSON(id),
  foreign key (position_id) references TBL_POSITION(id)
);

  
  