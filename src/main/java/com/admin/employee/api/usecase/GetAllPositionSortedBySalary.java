package com.admin.employee.api.usecase;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.domain.Position;
import com.admin.employee.api.entity.PositionEntity;
import com.admin.employee.api.mapper.impl.GetAllPSortedBySalaryMapper;
import com.admin.employee.api.service.PositionService;
import com.admin.employee.api.service.ex.PositionNotFoundException;
import com.admin.employee.api.service.ex.UnexpectedException;

public class GetAllPositionSortedBySalary {

	private final PositionService positionService;

	public GetAllPositionSortedBySalary(final PositionService positionService) {
		this.positionService = positionService;
	}

	public ResponseEntity<List<PositionEntity>> handleRequest() {
		try {
			List<Position> positions = positionService.getAllPositionEmployeesSortedBySalary(
					(Employee o1, Employee o2) -> o2.getSalary() - o1.getSalary());
			
			return new ResponseEntity<>(new GetAllPSortedBySalaryMapper().mapper(positions), HttpStatus.OK);
		} catch (PositionNotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (UnexpectedException err) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
