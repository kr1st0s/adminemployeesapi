package com.admin.employee.api.usecase;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.admin.employee.api.entity.PositionEntity;
import com.admin.employee.api.service.PositionService;

@Scope("singleton")
@Component
public class UseCasePositionHandle {

	private final PositionService positionService;

	public UseCasePositionHandle(PositionService positionService) {
		this.positionService = positionService;
	}

	public ResponseEntity<List<PositionEntity>> execute() {
		return new GetAllPositionSortedBySalary(positionService).handleRequest();
	}

}
