package com.admin.employee.api.usecase;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.mapper.impl.EmployeeEntityToEmployeeMapper;
import com.admin.employee.api.mapper.impl.EmployeeListToEmployeeEntityListMapper;
import com.admin.employee.api.mapper.impl.EmployeeToEmployeeEntityMapper;
import com.admin.employee.api.service.EmployeeService;
import com.admin.employee.api.service.ex.NotFoundEmployeeException;
import com.admin.employee.api.service.ex.TwoJobsIsNowAllowedException;
import com.admin.employee.api.service.ex.UnexpectedException;

@Component
public class UseCaseEmployeeHandle {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UseCaseEmployeeHandle.class);

	private final EmployeeService employeeService;

	public UseCaseEmployeeHandle(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public ResponseEntity<EmployeeEntity> addEmployee(EmployeeEntity employeeEntity) {
		try {
			Employee e = employeeService.save(new EmployeeEntityToEmployeeMapper().mapper(employeeEntity));
			return new ResponseEntity<>(new EmployeeToEmployeeEntityMapper().mapper(e), HttpStatus.OK);
		} catch (TwoJobsIsNowAllowedException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} catch (UnexpectedException err) {
			LOGGER.error("fail to try save employee to database ", err);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<EmployeeEntity> updateEmployee(EmployeeEntity employeeEntity) {
		try {
			Employee e = employeeService.update(new EmployeeEntityToEmployeeMapper().mapper(employeeEntity));
			return new ResponseEntity<>(new EmployeeToEmployeeEntityMapper().mapper(e), HttpStatus.OK);
		} catch (NotFoundEmployeeException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} catch (UnexpectedException err) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public ResponseEntity<Void> deleteEmployee(Long id) {
		try {
			employeeService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotFoundEmployeeException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (UnexpectedException err) {
			LOGGER.error("fail to try delete employee to database ", err);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ResponseEntity<List<EmployeeEntity>> findEmployeesByPositionOrPersonName(Long chargeId,  String personName){
		List<Employee> employeeList = employeeService.findEmployeeListByPositionOrPersonName(chargeId, personName);
		return new ResponseEntity<>(new EmployeeListToEmployeeEntityListMapper().mapper(employeeList), HttpStatus.OK); 
	}

}
