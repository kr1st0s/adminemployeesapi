package com.admin.employee.api.utils;

public class Utils {

	private Utils() {

	}

	public static boolean isStringNull(String value) {
		return value == null || value.equals(Constants.EMPTY);
	}

	public static boolean isNumericValid(Long value) {
		return value != null && value > 0;
	}

}
