package com.admin.employee.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.admin.employee.api.domain.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	List<Employee> findByPositionIdOrPersonFirstNameLike(Long positionId, String personName);

	Optional<Employee> findByPersonId(Long personId);

}
