package com.admin.employee.api.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

public class EmployeeEntity implements Serializable {

	private static final long serialVersionUID = -1313953593354430616L;

	private Long id;

	@NotNull
	private Integer salary;

	@NotNull
	private PersonEntity person;

	@NotNull
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private PositionEntity position;

	public EmployeeEntity(Long id, Integer salary, PersonEntity person, PositionEntity position) {
		this.id = id;
		this.salary = salary;
		this.person = person;
		this.position = position;
	}

	public EmployeeEntity() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}

	public PositionEntity getPosition() {
		return position;
	}

	public void setPosition(PositionEntity position) {
		this.position = position;
	}

}
