package com.admin.employee.api.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

public class PersonEntity implements Serializable {

	private static final long serialVersionUID = -2578913984898590129L;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private String name;

	private String lastName;

	private String address;

	private String cellPhone;

	private String cityName;

	public PersonEntity(String name, String lastName, String address, String cellPhone, String cityName) {
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.cellPhone = cellPhone;
		this.cityName = cityName;
	}

	public PersonEntity() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
