package com.admin.employee.api.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class PositionEntity implements Serializable {

	private static final long serialVersionUID = -1460059251441031833L;

	private Long id;

	private String name;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<EmployeeEntity> employees;

	public PositionEntity() {
	}

	public PositionEntity(Long id, String name, List<EmployeeEntity> employees) {
		this.id = id;
		this.name = name;
		this.employees = employees;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EmployeeEntity> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeEntity> employees) {
		this.employees = employees;
	}

}
