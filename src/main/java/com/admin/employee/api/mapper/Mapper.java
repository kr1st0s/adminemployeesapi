package com.admin.employee.api.mapper;

public interface Mapper <T, R>{

	 R mapper(T t);
	
}
