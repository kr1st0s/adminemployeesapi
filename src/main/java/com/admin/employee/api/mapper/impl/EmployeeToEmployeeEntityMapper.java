package com.admin.employee.api.mapper.impl;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.entity.PersonEntity;
import com.admin.employee.api.entity.PositionEntity;
import com.admin.employee.api.mapper.Mapper;

public class EmployeeToEmployeeEntityMapper  implements Mapper<Employee, EmployeeEntity>{

	@Override
	public EmployeeEntity mapper(Employee t) {
		EmployeeEntity employeeEntity = new EmployeeEntity();
		employeeEntity.setId(t.getId());
		employeeEntity.setSalary(t.getSalary());
		
		employeeEntity.setPerson(new PersonEntity());
		employeeEntity.getPerson().setAddress(t.getPerson().getAddress());
		employeeEntity.getPerson().setId(t.getPerson().getId());
		employeeEntity.getPerson().setCellPhone(t.getPerson().getCellPhone());
		employeeEntity.getPerson().setName(t.getPerson().getFirstName());
		employeeEntity.getPerson().setLastName(t.getPerson().getLastName());
		employeeEntity.getPerson().setCityName(t.getPerson().getCityName());
		
		employeeEntity.setPosition(new PositionEntity());
		employeeEntity.getPosition().setId(t.getPosition().getId());
		employeeEntity.getPosition().setName(t.getPosition().getPositionName());
		
		return employeeEntity;

	}

}
