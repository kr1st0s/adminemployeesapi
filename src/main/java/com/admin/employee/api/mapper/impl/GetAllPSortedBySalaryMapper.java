package com.admin.employee.api.mapper.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.admin.employee.api.domain.Position;
import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.entity.PersonEntity;
import com.admin.employee.api.entity.PositionEntity;
import com.admin.employee.api.mapper.Mapper;

public class GetAllPSortedBySalaryMapper implements Mapper<List<Position>, List<PositionEntity>> {

	@Override
	public List<PositionEntity> mapper(List<Position> positionList) {

		return positionList.stream().map(p -> new PositionEntity(p.getId(), p.getPositionName(),
				p.getEmployees().stream().map(e -> new EmployeeEntity(e.getId(), e.getSalary(),
						new PersonEntity(e.getPerson().getFirstName(), e.getPerson().getLastName(),
								e.getPerson().getAddress(), e.getPerson().getCellPhone(), e.getPerson().getCityName()),
						null)).collect(Collectors.toList())))
				.collect(Collectors.toList());
	}

}
