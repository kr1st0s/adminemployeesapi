package com.admin.employee.api.mapper.impl;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.domain.Person;
import com.admin.employee.api.domain.Position;
import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.mapper.Mapper;

public class EmployeeEntityToEmployeeMapper implements Mapper<EmployeeEntity, Employee>{

	@Override
	public Employee mapper(EmployeeEntity t) {
		Employee employee = new Employee();
		employee.setId(t.getId());
		employee.setSalary(t.getSalary());
		
		employee.setPerson(new Person());
		employee.getPerson().setAddress(t.getPerson().getAddress());
		employee.getPerson().setId(t.getPerson().getId());
		employee.getPerson().setCellPhone(t.getPerson().getCellPhone());
		employee.getPerson().setFirstName(t.getPerson().getName());
		employee.getPerson().setLastName(t.getPerson().getLastName());
		employee.getPerson().setCityName(t.getPerson().getCityName());
		
		employee.setPosition(new Position());
		employee.getPosition().setId(t.getPosition().getId());
		employee.getPosition().setPositionName(t.getPosition().getName());
		
		return employee;
	}

}
