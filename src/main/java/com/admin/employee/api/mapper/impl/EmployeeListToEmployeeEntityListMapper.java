package com.admin.employee.api.mapper.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.mapper.Mapper;

public class EmployeeListToEmployeeEntityListMapper implements Mapper<List<Employee>, List<EmployeeEntity>> {

	private static EmployeeToEmployeeEntityMapper toEmployeeEntity = new EmployeeToEmployeeEntityMapper();

	@Override
	public List<EmployeeEntity> mapper(List<Employee> t) {
		return t.stream().map(e -> toEmployeeEntity.mapper(e)).collect(Collectors.toList());
	}

}
