package com.admin.employee.api.web;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.admin.employee.api.entity.PositionEntity;
import com.admin.employee.api.usecase.UseCasePositionHandle;

@RequestMapping("/api/admin/v1/position")
@RestController
public class PositionWeb {

	private final UseCasePositionHandle useCasePositionHandle;

	public PositionWeb(final UseCasePositionHandle useCasePositionHandle) {
		this.useCasePositionHandle = useCasePositionHandle;
	}

	@GetMapping("/list")
	public ResponseEntity<List<PositionEntity>> getAllPositionSortedBySalary() {
		return useCasePositionHandle.execute();
	}

}
