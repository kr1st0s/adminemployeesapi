package com.admin.employee.api.web;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.admin.employee.api.entity.EmployeeEntity;
import com.admin.employee.api.usecase.UseCaseEmployeeHandle;

@RequestMapping("/api/admin/v1/employee")
@RestController
public class EmployeeWeb {

	private final UseCaseEmployeeHandle useCaseEmployeeHandle;

	@Autowired
	public EmployeeWeb(final UseCaseEmployeeHandle useCaseEmployeeHandle) {
		this.useCaseEmployeeHandle = useCaseEmployeeHandle;
	}

	@Transactional
	@PostMapping("/save")
	public ResponseEntity<EmployeeEntity> saveEmployee(@Valid @RequestBody EmployeeEntity employeeEntity,
			BindingResult result) {
		if (result.hasErrors()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return useCaseEmployeeHandle.addEmployee(employeeEntity);
	}

	@PutMapping("/update")
	public ResponseEntity<EmployeeEntity> updateEmployee(@Valid @RequestBody EmployeeEntity employeeEntity,
			BindingResult result) {
		if (result.hasErrors()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return useCaseEmployeeHandle.updateEmployee(employeeEntity);
	}

	@DeleteMapping(value = "/delete", params = "employeeId")
	public ResponseEntity<Void> deleteEmployee(@RequestParam Long employeeId) {
		return useCaseEmployeeHandle.deleteEmployee(employeeId);
	}

	@GetMapping(value = "/find", params = { "chargeId", "personName" })
	public ResponseEntity<List<EmployeeEntity>> getEmployeesByChargeIdOrPersonName(
			@RequestParam(required = false) Long chargeId, @RequestParam(required = false) String personName) {
		return useCaseEmployeeHandle.findEmployeesByPositionOrPersonName(chargeId, personName);
	}

}
