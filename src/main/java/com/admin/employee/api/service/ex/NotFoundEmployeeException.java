package com.admin.employee.api.service.ex;

public class NotFoundEmployeeException extends ServiceException{

	private static final long serialVersionUID = 1830283196167256744L;

	public NotFoundEmployeeException(String message) {
		super(message);
	}
	
	public NotFoundEmployeeException(String message, Throwable cause) {
		super(message, cause);
	}

}
