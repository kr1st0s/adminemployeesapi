package com.admin.employee.api.service;

import java.util.Comparator;
import java.util.List;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.domain.Position;
import com.admin.employee.api.service.ex.PositionNotFoundException;

public interface PositionService {

	public List<Position> getAllPositionEmployeesSortedBySalary(Comparator<Employee> sortBy) throws PositionNotFoundException;

}
