package com.admin.employee.api.service.ex;

public class UnexpectedException extends RuntimeException {

	private static final long serialVersionUID = -8582775559184950955L;
	
	public UnexpectedException(String message, Throwable cause) {
		super(message, cause);
	}

}
