package com.admin.employee.api.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.domain.Position;
import com.admin.employee.api.repository.PositionRepository;
import com.admin.employee.api.service.PositionService;
import com.admin.employee.api.service.ex.PositionNotFoundException;
import com.admin.employee.api.service.ex.UnexpectedException;

@Scope("singleton")
@Service
public class PositionServiceImpl implements PositionService {

	private final PositionRepository positionRepository;
	@Autowired
	public PositionServiceImpl(PositionRepository positionRepository) {
		this.positionRepository = positionRepository;
	}

	@Override
	public List<Position> getAllPositionEmployeesSortedBySalary(Comparator<Employee> sort) throws PositionNotFoundException {
		try {
			List<Position> positionList = positionRepository.findAll();
			if (positionList.isEmpty()) {
				throw new PositionNotFoundException("Not found positions");
			}
			positionList.forEach(p -> Collections.sort(p.getEmployees(), sort));
			return positionList;
		} catch (Exception cause) {
			throw new UnexpectedException("unexpected exception with root cause: ", cause);
		}
	}

}
