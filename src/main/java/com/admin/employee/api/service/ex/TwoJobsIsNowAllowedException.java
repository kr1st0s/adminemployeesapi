package com.admin.employee.api.service.ex;

public class TwoJobsIsNowAllowedException extends ServiceException {

	private static final long serialVersionUID = 4771879754950222231L;

	public TwoJobsIsNowAllowedException(String message) {
		super(message);
	}

}
