package com.admin.employee.api.service.ex;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 1419810783714176105L;
	
	public ServiceException(String message, Throwable err) {
		super(message, err);
	}

	public ServiceException(String message) {
		super(message);
	}

}
