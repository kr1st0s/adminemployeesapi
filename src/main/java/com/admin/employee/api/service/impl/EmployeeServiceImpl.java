package com.admin.employee.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.repository.EmployeeRepository;
import com.admin.employee.api.service.EmployeeService;
import com.admin.employee.api.service.ex.NotFoundEmployeeException;
import com.admin.employee.api.service.ex.TwoJobsIsNowAllowedException;
import com.admin.employee.api.service.ex.UnexpectedException;
import com.admin.employee.api.utils.Utils;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;

	}

	@Override
	public Optional<Employee> findById(Long id) {
		try {
			return employeeRepository.findById(id);
		} catch (Exception cause) {
			throw new UnexpectedException("Unexpected exception with root cause: ", cause);
		}
	}

	@Override
	public Employee save(Employee domain) throws TwoJobsIsNowAllowedException {
		try {
			if (employeeRepository.findByPersonId(domain.getPerson().getId()).isPresent()) {
				throw new TwoJobsIsNowAllowedException("Only is allow one job by person");
			}
			return employeeRepository.save(domain);
		} catch (Exception cause) {
			throw new UnexpectedException("Unexpected exception with root cause: ", cause);
		}
	}

	@Override
	public Employee update(Employee domain) throws NotFoundEmployeeException {
		try {
			Optional<Employee> eOptional = employeeRepository.findById(domain.getId());
			if (!eOptional.isPresent())
				throw new NotFoundEmployeeException("not found employee");
			Employee e = eOptional.get();
			e.setPerson(domain.getPerson());
			e.setPosition(domain.getPosition());
			e.setSalary(domain.getSalary());
			return employeeRepository.save(e);
		} catch (Exception cause) {
			throw new UnexpectedException("Unexpected exception with root cause: ", cause);
		}
	}

	@Modifying
	@Override
	public void delete(Long id) throws NotFoundEmployeeException {
		try {
			employeeRepository.deleteById(id);
		} catch (DataAccessException accesException) {
			throw new NotFoundEmployeeException("Unexpected exception with root cause: ", accesException);
		} catch (Exception cause) {
			throw new UnexpectedException("Unexpected exception with root cause: ", cause);
		}
	}

	@Override
	public List<Employee> findEmployeeListByPositionOrPersonName(Long chargeId, String chargeName) {
		if (!Utils.isNumericValid(chargeId) ||  Utils.isStringNull(chargeName)) {
			return employeeRepository.findAll();
		}
		return employeeRepository.findByPositionIdOrPersonFirstNameLike(chargeId, chargeName);
	}

}
