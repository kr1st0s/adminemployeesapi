package com.admin.employee.api.service;

import java.util.List;
import java.util.Optional;

import com.admin.employee.api.domain.Employee;
import com.admin.employee.api.service.ex.NotFoundEmployeeException;
import com.admin.employee.api.service.ex.TwoJobsIsNowAllowedException;

public interface EmployeeService {

	public Optional<Employee> findById(Long id);

	public Employee save(Employee employee) throws TwoJobsIsNowAllowedException;

	public Employee update(Employee employee) throws NotFoundEmployeeException;

	public void delete(Long id) throws NotFoundEmployeeException;

	public List<Employee> findEmployeeListByPositionOrPersonName(Long chargeId, String chargeName);

}
