package com.admin.employee.api.service.ex;

public class PositionNotFoundException extends ServiceException {

	private static final long serialVersionUID = -5696966314789375590L;

	public PositionNotFoundException(String message, Throwable err) {
		super(message, err);

	}

	public PositionNotFoundException(String message) {
		super(message);
	}

}
